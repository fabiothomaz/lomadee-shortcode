<?php
/**
 * Plugin Name: Vitrine de ofertas da Lomadee via Wordpress Shortcode
 * Description: Cria área para exibição de uma vitrine de ofertas 
 *              Lomadee dentro de um post no wordpress. Baseado no tutorial 
 *              para criação do plugin/widget Ofertas Relacionadas, disponível em:
 *              http://developer.buscape.com/tutoriais/como-criar-um-widget-para-wordpress-para-exibir-ofertas-da-lomadee/
 * Version: 1.0
 */

class Lomadee_Shortcode {

    /**
     * ID da aplicação no Dev Buscapé
     */
    private $application_id = '5a4f6243415636535869413d';

    /**
     * Define se a API do Buscapé deve utilizar a busca de ofertas na Lomadee
     */
    public $lomadee = true;

    /*
     * Atributo para acessar as opções do plugin
     */
    private $options;

    public function __construct()
    {
        // Exibe o CSS aplicado para este plugin
        add_action( 'wp_print_styles', array( &$this, 'stylesheet' ) );

        // Inclui a página de opções do plugin
        add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );

        // Registra e inclui as opções
        add_action( 'admin_init', array( $this, 'page_init' ) );

        // Inclui o shortcode [lomadee]
        add_shortcode( 'lomadee', array( $this, 'parse_lomadee_shortcode' ) );       

        // Inclui o shortcode [lomadee_best_offer]
        add_shortcode( 'lomadee_best_offer', array( $this, 'parse_lomadee_best_offer_shortcode' ) );       
    }

    /**
     * Inclui a página de opções
     */
    public function add_plugin_page()
    {
        // Esta página ficará no menu "Opções -> Lomadee Shortcode"
        add_options_page(
                'Settings Admin', 
                'Lomadee Shortcode', 
                'manage_options', 
                'lomadee-shortcode-admin', 
                array( $this, 'create_admin_page' )
                );
    }

    /**
     * Callback da página de opções
     */
    public function create_admin_page()
    {
        // Busca o atributo lomadee_options
        $this->options = get_option( 'lomadee_shortcode_options' );
        ?>
        <div class="wrap">
        <?php screen_icon(); ?>
            <h2>Lomadee</h2>           
            <form method="post" action="options.php">
        <?php
            settings_fields( 'lomadee_shortcode_option_group' );   
            do_settings_sections( 'lomadee-shortcode-admin' );
            submit_button(); 
        ?>
            </form>
        </div>
        <?php
    }    

    /**
     * Registra e inclui as opções 
     */
    public function page_init()
    {        
        register_setting(
                'lomadee_shortcode_option_group', // Option group
                'lomadee_shortcode_options', // Option name
                array( $this, 'sanitize' ) // Sanitize
                );

        add_settings_section(
                'setting_section_id', // ID
                'Configurações Lomadee Shortcode', // Title
                array( $this, 'print_section_info' ), // Callback
                'lomadee-shortcode-admin' // Page
                );  

        add_settings_field(
                'source_id', // ID
                'Informe o seu SOURCE_ID', // Title 
                array( $this, 'source_id_callback' ), // Callback
                'lomadee-shortcode-admin', // Page
                'setting_section_id' // Section           
                );      

    }

    public function sanitize( $input )
    {
        $new_input = array();
        if( isset( $input['source_id'] ) )
            $new_input['source_id'] = sanitize_text_field( $input['source_id'] );

        return $new_input;
    }

    public function print_section_info()
    {
        print 'Configuração dos parâmetros da Lomadee:';
    }

    public function source_id_callback()
    {
        printf(
                '<input type="text" id="source_id" name="lomadee_shortcode_options[source_id]" value="%s" />',
                isset( $this->options['source_id'] ) ? esc_attr( $this->options['source_id']) : ''
              );
    }

    /**
     * Retorna o source_id salvo nas opções do Lomadee Shortcode
     */
    public function get_source_id() 
    {
        return $this->options['source_id'];
    }

    /**
     * Registra o style do plugin
     *
     * @return void
     */
    public function stylesheet()
    {
        wp_enqueue_style( 'lomadee-shortcode-style', WP_PLUGIN_URL . '/lomadee-shortcode/assets/css/style.css' );
    }

    /**
     * Método utilizado para o shortcode (possibilidade de incluir o painel de ofertas dentro de um post)
     *
     * @param array $atts Parâmetros informados no shortcode [lomadee]
     */
    public function parse_lomadee_shortcode( $atts )
    {
        extract( shortcode_atts( 
                    array( 'keyword' => 'smartphone', 
                        'limit' => '5',
                        'product_id' => null,
                        ), $atts ));       

        $instance = [];
        $instance['source_id'] = $this->get_source_id();
        
        // Se tiver o Id do produto, não usa o atributo keyword
        if (!empty($product_id)) 
            $instance['productId'] = $product_id;
        else
            $instance['keyword'] = $keyword;

        $instance['limit'] = $limit;

        $offers = $this->get_offers( $instance );

        ob_start(); 
	if (count($offers) > 0) {
	    $this->display_offers( $offers );
	}
        return ob_get_clean();
    }

    /**
     * Método utilizado para o shortcode (possibilidade de incluir o painel de ofertas dentro de um post)
     *
     * @param array $atts Parâmetros informados no shortcode [lomadee]
     */
    public function parse_lomadee_best_offer_shortcode( $atts )
    {
        extract( shortcode_atts( 
                    array( 'keyword' => 'smartphone', 
                        'product_id' => null,
                        ), $atts ));       

        $instance = [];
        $instance['source_id'] = $this->get_source_id();
        
        // Se tiver o Id do produto, não usa o atributo keyword
        if (!empty($product_id)) 
            $instance['productId'] = $product_id;
        else
            $instance['keyword'] = $keyword;

        $offers = $this->get_offers( $instance, false );

        $best_offer = null;
        foreach ($offers as $offer) {
            if (empty($best_offer) || $offer['price'] < $best_offer['price']) {
                $best_offer = $offer;
            }
        }

        ob_start(); 
	if (count($offers) > 0) {
            $this->display_offers( array($best_offer), "Melhor Oferta Encontrada" );
	}
        return ob_get_clean();
    }

    /**
     * Monta o html para visualizacao das ofertas
     *
     * @param array $offers Lista de ofertas
     */
    public function display_offers( $offers, $title="Ofertas Disponíveis" )
    {
        ?>
        <div class="lomadee-shortcode-offers">
            <h2><?php echo $title; ?></h2>
            <div class="lomadee-shortcode-offers-list">
                <ul>
                <?php foreach( (array)$offers as $offer ) : ?>
                    <li>
                        <div class="lomadee-shortcode-offers-item">
                            <div class="lomadee-shortcode-offers-item-image">
                                <a href="<?php echo $offer['link']; ?>">
                                    <image src="<?php echo $offer['image']; ?>" width="120" height="120" alt="<?php echo $offer['name']; ?>" />
                                </a>
                            </div>
                            <div class="lomadee-shortcode-offers-item-name">
                                <p class="lomadee-shortcode-offers-item-data-name">
                                    <a href="<?php echo $offer['link']; ?>"><?php echo $offer['name']; ?></a>
                                </p>
                            </div>
                            <div class="lomadee-shortcode-offers-seller-image">
                                <image src="<?php echo $offer['sellerImage']; ?>" width="120" height="120" alt="<?php echo $offer['sellerName']; ?>" />
                            </div>
                            <div class="lomadee-shortcode-offers-item-price">
                                <p class="lomadee-shortcode-offers-item-data-price">
                                    Preço:<br/>
                                    <a href="<?php echo $offer['link']; ?>">R$ <?php echo number_format( $offer['price'], 2, ',', '.' ); ?></a>
                                </p>
                            </div>
                        </div>
                        <div style="clear: both"></div>
                    </li>
                <?php endforeach; ?>
                </ul>
            </div>
        </div>
        <?php
    }

    /**
     * Obtem as ofertas de acordo com as especificações definidas 
     *
     * @param array $instance Instancia dos dados do plugin
     * @return array Ofertas
     */
    public function get_offers( $instance, $apply_limit=true )
    {
        require_once 'includes/buscape-php/Apiki_Buscape_API.php';

        // Instancia o wrapper para PHP da API do BuscaPé
        $obj_wrapper_php = new Apiki_Buscape_API( $this->application_id, $instance['source_id'] );

        // Habilita o ambiente de testes e desenvolvimento da API do BuscaPé.
        // Quando o plugin estiver em produção, esta linha deve ser removida.
        // $obj_wrapper_php->setSandbox();

        // Define JSON como formato de retorno
        $obj_wrapper_php->setFormat( 'json' );

        // Busca as ofertas de acordo com as palavras-chave
        $offers = $obj_wrapper_php->findOfferList( $instance, $this->lomadee );

        // Trata o retorno das ofertas em JSON
        $offers = json_decode( $offers );

        // Trata e prepara um array com os dados das ofertas
        $prepared_offers = $this->prepare_offers( $offers );

        // Limita os resultados
        if ($apply_limit) {
            $prepared_offers = array_slice( $prepared_offers, 0, $instance['limit'] );
        }

        return $prepared_offers;
    }

    /**
     * Trata e prepara os dados das ofertas para uma melhor leitura na exibição
     *
     * @param array $offers
     * @return boolean
     */
    public function prepare_offers( $offers )
    {
        if( !isset( $offers->offer ) )
            return false;

        if( empty( $offers->offer ) )
            return false;

        $prepared_offers = array();
        $seller_list = array();

        foreach( $offers->offer as $offer ) :

            // Obtém somente ofertas com imagens
            if( !isset( $offer->offer->thumbnail->url ) )
                continue;

            // Obtém somente ofertas de lojas com imagens
            if( !isset( $offer->offer->seller->thumbnail->url ) )
                continue;

            if (in_array($offer->offer->seller->sellername, $seller_list))
                continue;
            else
                array_push($seller_list, $offer->offer->seller->sellername);

            $prepared_offers[] = array(
                'id'    => $offer->offer->id,
                'name'  => $offer->offer->offername,
                'image' => $offer->offer->thumbnail->url,
                'link'  => $offer->offer->links[0]->link->url,
                'price' => $offer->offer->price->value,
                'sellerImage' => $offer->offer->seller->thumbnail->url,
                'sellerName' => $offer->offer->seller->sellername,
                );

        endforeach;
        
        usort($prepared_offers, array( &$this, 'compare_usort' ));

        return $prepared_offers;
    }

    private function compare_usort($a, $b)
    {   
        if ($a['price'] == $b['price']) {
        return 0;
        }
        return ($a['price'] < $b['price']) ? -1 : 1;
    }
}

// Cria uma instancia do plugin e o executa
$lomadee_shortcode = new Lomadee_Shortcode();

?>
